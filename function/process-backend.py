import os
import pathlib
import subprocess
import boto3
import base64
import json
import hashlib
import hmac
import shutil
import uuid
import requests
import traceback

from git import Repo
# from dotenv import load_dotenv

# load_dotenv()

join = os.path.join
gitea_key = os.environ.get('GITEA_SSH_KEY')
codecommit_key = os.environ.get('CODECOMMIT_KEY')
sns_topic_arn = os.environ.get('SNS_TOPIC')
email_sns_topic_arn = os.environ.get('EMAIL_SNS_TOPIC')
codecommit_ssh = os.environ.get('CODECOMMIT_SSH')
secret = os.environ.get('SECRET')
# key_filepath = 'D:/git-key.txt'
key_filepath = '/tmp/git-key.txt'


def get_env(repo_path):
  my_env = os.environ.copy()
  local_node_modules_bin_dir = join(repo_path, 'node_modules', '.bin')
  my_env['NPM_CONFIG_USERCONFIG'] = '/tmp/.npmrc'
  my_env['NODE_OPTIONS'] = '--max_old_space_size=8192'
  if (local_node_modules_bin_dir not in my_env['PATH']):
    my_env['PATH'] = local_node_modules_bin_dir + ':' + my_env['PATH']
    subprocess.run(['npm', 'config', 'set', 'cache', '/tmp/.npm'], env=my_env)
  return my_env

def get_git_key(key):
  with open(key_filepath, 'w') as f:
    f.write(key)
  subprocess.run(['chmod', '400', key_filepath])


def delete_git_key(filepath):
  print('start delete_git_key')
  if os.path.exists(filepath):
    os.remove(filepath)

def send_email(body, title):
  sns_client = boto3.client('sns')
  response = sns_client.publish(TargetArn=email_sns_topic_arn, Message='{0}\n{1}'.format(title, body))
  print('publish to email sns')
  print(response)
  
def compute_signature(payload_bytes, secret):
  m = hmac.new(key=secret.encode(), msg=payload_bytes, digestmod=hashlib.sha256)
  return 'sha256=' + m.hexdigest()
  
def send_gitea_comment(message, payload):
  if 'pull_request' in payload:
    pull_request_number = payload['pull_request']['number']
    owner = payload['repository']['owner']['login']
    repo = payload['repository']['name']
    
    gitea_payload = {
      'pullRequestNumber': pull_request_number,
      'owner': owner,
      'repo': repo,
      'message': message
    }
    
    json_bytes = json.dumps(gitea_payload).encode('utf-8')
    signature = compute_signature(json_bytes, secret)
    
    sns_payload = {
      'signature': signature,
      'body': json.dumps(gitea_payload),
      'base64Body': base64.b64encode(json_bytes).decode('utf-8'),
      'repo': repo
    }
    
    print('sns_payload')
    print(sns_payload)
    
    sns_client = boto3.client('sns', region_name='us-east-1')
    response = sns_client.publish(TargetArn=sns_topic_arn, Message=json.dumps({ 'default': json.dumps(sns_payload ) }), MessageStructure='json')
    print('publish to sns')
    print(response)

def get_file_type(filename):
    try:
        # Run the file command
        result = subprocess.run(['file', filename], stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
        # Decode the output to get a string
        output = result.stdout.decode('utf-8').strip()
        return output
    except subprocess.CalledProcessError as e:
        print(f"Error calling file command: {e}")
        return None

def fix_package_json(repo_path):
  print('start fix_package_json')
  shutil.copyfile(join(repo_path, 'package.json'), join(repo_path, 'package-orig.json'))
  
  with open(join(repo_path, 'package.json'), 'r') as f:
    data = json.loads(f.read())
  
  if 'cypress' in data['devDependencies']:
    del data['devDependencies']['cypress']
    with open(join(repo_path, 'package.json'), 'w') as f:
      json.dump(data, f)
  


def clone(url, branch='main', commit_id = None):
  print('clone function start to run')
  print('url')
  print(url)
  # branch='main'
  print('branch')
  print(branch)
  # commit_id='2497a096a3c1975bcb87d3becc8e4c542cdc0c68'
  print('commit_id')
  print(commit_id)
  cwd = os.getcwd()
  tmp_id = str(uuid.uuid4())[:8]
  repo_path = '/tmp/{0}'.format(tmp_id)
  # repo_path = 'D:/tmp/{0}'.format(tmp_id)
  get_git_key(gitea_key.strip())

  if commit_id is None:
    repo = Repo.clone_from(
      url,
      repo_path,
      branch=branch,
      env={
        'GIT_SSH_COMMAND': 'ssh -v -o StrictHostKeyChecking=no -o UserKnownHostsFile=/tmp/known_hosts -i {0}'.format(key_filepath)
      }
    )
  else:
    repo = Repo.clone_from(
      url,
      repo_path,
      no_checkout=True,
      env={
        'GIT_SSH_COMMAND': 'ssh -v -o StrictHostKeyChecking=no -o UserKnownHostsFile=/tmp/known_hosts -i {0}'.format(key_filepath)
      }
    )
    repo.git.checkout(commit_id)
  
  fix_package_json(repo_path)
  delete_git_key(key_filepath)
  print('clone function finish')

  return repo_path
  
def build_message_header(payload, branch='main'):
  if 'pull_request' in payload:
    title = payload['pull_request']['title']
    body = payload['pull_request']['body']
    from_user = payload['pull_request']['user']['username']
    from_branch = payload['pull_request']['head']['ref']
    return 'Pull Request: {0}\nBody: {1}\nBranch: {2}\nUser: {3}'.format(title, body, from_branch, from_user)
    
  if 'pusher' in payload:
    return 'Body: {0}\nBranch: {1}\nUser: {2}'.format(payload['head_commit']['message'], branch, payload['pusher']['username'])
  
def build(args, email_title, payload, repo_path, branch):
  proc = subprocess.run(args, cwd=repo_path, capture_output=True, text=True, env=get_env(repo_path))

  if (proc.returncode != 0):
    message_header = build_message_header(payload)
    email_body = '{0}\n\nError:\n{1}\n{2}'.format(message_header, proc.stdout, proc.stderr)
    send_email(email_body, email_title)
    
    if 'Lint' in email_title:
      send_gitea_comment(message='Lint Errors:\n{0}\n{1}'.format(proc.stdout, proc.stderr), payload=payload)
    elif 'Build' in email_title:
      send_gitea_comment(message='Build Errors:\n{0}\n{1}'.format(proc.stdout, proc.stderr), payload=payload)
    elif 'Test' in email_title:
      send_gitea_comment(message='Test Errors:\n{0}\n{1}'.format(proc.stdout, proc.stderr), payload=payload)
  
  return proc.returncode

def build_all_backend(payload, repo_path, branch):
  print('start build_all_backend')
  print('start install package')
  # subprocess.run(['C:/Program Files/nodejs/npm.cmd', 'i', '--force'], cwd=repo_path)
  subprocess.run(['npm', 'i', '--force'], cwd=repo_path, env=get_env(repo_path))

  print('start lint')  
  # lint_return = build(['C:/Program Files/nodejs/npm.cmd', 'run', 'lint'], email_title='Backend Lint Failed', payload=payload, repo_path=repo_path, branch=branch)
  lint_return = build(['npm', 'run', 'lint'], email_title='Backend Lint Failed', payload=payload, repo_path=repo_path, branch=branch)
  print('lint_return') 
  print(lint_return) 
  if (lint_return != 0):
    return False

  print('start testing')  
  # test_return = build(['C:/Program Files/nodejs/npm.cmd', 'run', 'test'], email_title='Backend Test Failed', payload=payload, repo_path=repo_path, branch=branch)
  test_return = build(['npm', 'run', 'test'], email_title='Backend Test Failed', payload=payload, repo_path=repo_path, branch=branch)
  print('test_return') 
  print(test_return) 
  if (test_return != 0):
    return False
  
  print('start build')  
  # build_return = build(['C:/Program Files/nodejs/npm.cmd', 'run', 'build'], email_title='Backend Build Failed', payload=payload, repo_path=repo_path, branch=branch)
  build_return = build(['npm', 'run', 'build'], email_title='Backend Build Failed', payload=payload, repo_path=repo_path, branch=branch)
  print('build_return') 
  print(build_return) 
  if (build_return != 0):
    return False
  
  return True


  
def process_backend(payload, branch, commit_id):
  repo_path = clone('git@gitea.com:khainguyen1203/test-project.git', branch=branch, commit_id=commit_id)
  print('repo_path')
  print(repo_path)
  try:
    ok = build_all_backend(payload, repo_path, branch)
    print("is build ok")
    print(ok)
    message_header = build_message_header(payload, branch)
  
    if ok:
      email_body = '{0}\n\nBuild Ok'.format(message_header)
      send_email(email_body, 'Backend Build Ok')
      send_gitea_comment(message='Build Ok', payload=payload)
  except Exception as e:
    print('Got exception')
    print(e)
    print(traceback.format_exc())
  finally:
  # this line will remove file in repo_path
    shutil.rmtree(repo_path, ignore_errors=True)
  
def event_handler(payload):
  print(json.dumps(payload))
  branch = None
  commit_id = None
  
  if (payload['repository']['name'] == 'test-project'):
    if ('pull_request' in payload and payload['action'] in ('opened', 'edited', 'reopened', 'synchronized')):
      branch = payload['pull_request']['head']['label']
      commit_id = payload['pull_request']['head']['sha']
    if branch is not None:
      try:
        process_backend(payload, branch, commit_id)
      except Exception as e:
        print(e)
        
        if ('pull_request' in payload):
          message_header = build_message_header(payload, branch)
          email_title = '2R Backend Build Failed'
          send_email('{0}\n\nPull request failed: {1}\nError: {2}'.format(message_header, payload['pull_request']['html_url'], e), email_title)

def get_queue_url():
  sqs_client = boto3.client("sqs")
  response = sqs_client.get_queue_url(
      # QueueName="test-project-inbound-queue.fifo",
      QueueName="test-project-inbound-queue",
  )
  return response["QueueUrl"]

def lambda_handler(event):
  print(json.dumps(event))
  sqs_client = boto3.client('sqs')
  queue_url = get_queue_url()

  event_handler(json.loads(event['body']))

  # Delete message after processing
  receiptHandle = event['receiptHandle']

  try:
    sqs_client.delete_message(
      QueueUrl=queue_url,
      ReceiptHandle=receiptHandle
    )
  except Exception as e:
    print(e)


def main():
  eventStr = os.environ.get('SQS_MESSAGE')
 

  for key, value in os.environ.items():
    print(f'{key}: {value}')
  print(f'eventStr: {eventStr}')
  event = json.loads(eventStr)
  # event = eventStr
  print(f'event json: {json.dumps(event)}')
  lambda_handler(event)

# clone('git@gitea.com:khainguyen1203/test-project.git', 'main', '2497a096a3c1975bcb87d3becc8e4c542cdc0c68')
main()


 # eventStr =  {'messageId': 'c08f46b0-bac6-40c5-98c2-0513bf1a9bdd', 'receiptHandle': 'AQEBcJPbV2EhsjlcISVYNr8JSYknoKTfV6sEpSLjE6aaWwssNOirJ8GRWuefj9zihs+5Ek/Je2OQqRcoWZWjAZb9YLB97e/yEzu7HAozkuc9/TCBu1w1xN9HihiKTXEnahiNzJwNn2YSXrG9n9mDhQVRNG1MUajfnHEB72uTADbtYRLoX8H90piPt7fH4gYPMtcitPp+h1S9dl/lREkgRR4WR29WQTqSev+zHPTsNjikRC+XG9hfIxF/fSPNUicZKeqO451Jrux0TubvXCrU7llfkQMZxs+t6q2NdaLdrkns3TdLPYfG5ybsl/rTxYvI2gwt/o/O4HHm3A500TWxyTSyl1z+sKx2ZNDMCATbE4AevYR8FDabUC9ZjIJS5/UiYvFiWuytRR2lECtXYx6Ugtcl5fS4T7wCvUecuJaibWJ0pg8=', 'body': '{"action": "opened", "number": 43, "pull_request": {"id": 69934, "url": "https://gitea.com/khainguyen1203/test-project/pulls/43", "number": 43, "user": {"id": 64084, "login": "khainguyen1203", "login_name": "", "full_name": "", "email": "khainguyen1203@noreply.gitea.com", "avatar_url": "https://seccdn.libravatar.org/avatar/a1bb6f5f3edfd0de113839ae0032b0db?d=identicon", "language": "", "is_admin": false, "last_login": "0001-01-01T00:00:00Z", "created": "2024-04-07T05:43:53Z", "restricted": false, "active": false, "prohibit_login": false, "location": "", "website": "", "description": "", "visibility": "public", "followers_count": 0, "following_count": 0, "starred_repos_count": 0, "username": "khainguyen1203"}, "title": "logs", "body": "", "labels": [], "milestone": null, "assignee": null, "assignees": null, "requested_reviewers": null, "state": "open", "is_locked": false, "comments": 0, "html_url": "https://gitea.com/khainguyen1203/test-project/pulls/43", "diff_url": "https://gitea.com/khainguyen1203/test-project/pulls/43.diff", "patch_url": "https://gitea.com/khainguyen1203/test-project/pulls/43.patch", "mergeable": true, "merged": false, "merged_at": null, "merge_commit_sha": null, "merged_by": null, "allow_maintainer_edit": false, "base": {"label": "main", "ref": "main", "sha": "ccd7f8e453d62d554a538a33cce9cbd065907e4d", "repo_id": 54638, "repo": {"id": 54638, "owner": {"id": 64084, "login": "khainguyen1203", "login_name": "", "full_name": "", "email": "khainguyen1203@noreply.gitea.com", "avatar_url": "https://seccdn.libravatar.org/avatar/a1bb6f5f3edfd0de113839ae0032b0db?d=identicon", "language": "", "is_admin": false, "last_login": "0001-01-01T00:00:00Z", "created": "2024-04-07T05:43:53Z", "restricted": false, "active": false, "prohibit_login": false, "location": "", "website": "", "description": "", "visibility": "public", "followers_count": 0, "following_count": 0, "starred_repos_count": 0, "username": "khainguyen1203"}, "name": "test-project", "full_name": "khainguyen1203/test-project", "description": "test-project", "empty": false, "private": false, "fork": false, "template": false, "parent": null, "mirror": false, "size": 287, "language": "", "languages_url": "https://gitea.com/api/v1/repos/khainguyen1203/test-project/languages", "html_url": "https://gitea.com/khainguyen1203/test-project", "url": "https://gitea.com/api/v1/repos/khainguyen1203/test-project", "link": "", "ssh_url": "git@gitea.com:khainguyen1203/test-project.git", "clone_url": "https://gitea.com/khainguyen1203/test-project.git", "original_url": "", "website": "", "stars_count": 0, "forks_count": 0, "watchers_count": 1, "open_issues_count": 0, "open_pr_counter": 0, "release_counter": 0, "default_branch": "main", "archived": false, "created_at": "2024-04-07T05:44:33Z", "updated_at": "2024-04-11T08:57:54Z", "archived_at": "1970-01-01T00:00:00Z", "permissions": {"admin": false, "push": false, "pull": true}, "has_issues": true, "internal_tracker": {"enable_time_tracker": true, "allow_only_contributors_to_track_time": true, "enable_issue_dependencies": true}, "has_wiki": true, "has_pull_requests": true, "has_projects": true, "projects_mode": "all", "has_releases": true, "has_packages": false, "has_actions": true, "ignore_whitespace_conflicts": false, "allow_merge_commits": true, "allow_rebase": true, "allow_rebase_explicit": true, "allow_squash_merge": true, "allow_fast_forward_only_merge": true, "allow_rebase_update": true, "default_delete_branch_after_merge": false, "default_merge_style": "merge", "default_allow_maintainer_edit": false, "avatar_url": "", "internal": false, "mirror_interval": "", "object_format_name": "", "mirror_updated": "0001-01-01T00:00:00Z", "repo_transfer": null}}, "head": {"label": "test2/function", "ref": "test2/function", "sha": "6cb500d20d974de0597c1fb6ee9c5d4c8240ff5b", "repo_id": 54638, "repo": {"id": 54638, "owner": {"id": 64084, "login": "khainguyen1203", "login_name": "", "full_name": "", "email": "khainguyen1203@noreply.gitea.com", "avatar_url": "https://seccdn.libravatar.org/avatar/a1bb6f5f3edfd0de113839ae0032b0db?d=identicon", "language": "", "is_admin": false, "last_login": "0001-01-01T00:00:00Z", "created": "2024-04-07T05:43:53Z", "restricted": false, "active": false, "prohibit_login": false, "location": "", "website": "", "description": "", "visibility": "public", "followers_count": 0, "following_count": 0, "starred_repos_count": 0, "username": "khainguyen1203"}, "name": "test-project", "full_name": "khainguyen1203/test-project", "description": "test-project", "empty": false, "private": false, "fork": false, "template": false, "parent": null, "mirror": false, "size": 287, "language": "", "languages_url": "https://gitea.com/api/v1/repos/khainguyen1203/test-project/languages", "html_url": "https://gitea.com/khainguyen1203/test-project", "url": "https://gitea.com/api/v1/repos/khainguyen1203/test-project", "link": "", "ssh_url": "git@gitea.com:khainguyen1203/test-project.git", "clone_url": "https://gitea.com/khainguyen1203/test-project.git", "original_url": "", "website": "", "stars_count": 0, "forks_count": 0, "watchers_count": 1, "open_issues_count": 0, "open_pr_counter": 0, "release_counter": 0, "default_branch": "main", "archived": false, "created_at": "2024-04-07T05:44:33Z", "updated_at": "2024-04-11T08:57:54Z", "archived_at": "1970-01-01T00:00:00Z", "permissions": {"admin": false, "push": false, "pull": true}, "has_issues": true, "internal_tracker": {"enable_time_tracker": true, "allow_only_contributors_to_track_time": true, "enable_issue_dependencies": true}, "has_wiki": true, "has_pull_requests": true, "has_projects": true, "projects_mode": "all", "has_releases": true, "has_packages": false, "has_actions": true, "ignore_whitespace_conflicts": false, "allow_merge_commits": true, "allow_rebase": true, "allow_rebase_explicit": true, "allow_squash_merge": true, "allow_fast_forward_only_merge": true, "allow_rebase_update": true, "default_delete_branch_after_merge": false, "default_merge_style": "merge", "default_allow_maintainer_edit": false, "avatar_url": "", "internal": false, "mirror_interval": "", "object_format_name": "", "mirror_updated": "0001-01-01T00:00:00Z", "repo_transfer": null}}, "merge_base": "6517618616b6b540448f971b8a684c8bb6196911", "due_date": null, "created_at": "2024-04-11T08:58:12Z", "updated_at": "2024-04-11T08:58:13Z", "closed_at": null, "pin_order": 0}, "requested_reviewer": null, "repository": {"id": 54638, "owner": {"id": 64084, "login": "khainguyen1203", "login_name": "", "full_name": "", "email": "khainguyen1203@noreply.gitea.com", "avatar_url": "https://seccdn.libravatar.org/avatar/a1bb6f5f3edfd0de113839ae0032b0db?d=identicon", "language": "", "is_admin": false, "last_login": "0001-01-01T00:00:00Z", "created": "2024-04-07T05:43:53Z", "restricted": false, "active": false, "prohibit_login": false, "location": "", "website": "", "description": "", "visibility": "public", "followers_count": 0, "following_count": 0, "starred_repos_count": 0, "username": "khainguyen1203"}, "name": "test-project", "full_name": "khainguyen1203/test-project", "description": "test-project", "empty": false, "private": false, "fork": false, "template": false, "parent": null, "mirror": false, "size": 287, "language": "", "languages_url": "https://gitea.com/api/v1/repos/khainguyen1203/test-project/languages", "html_url": "https://gitea.com/khainguyen1203/test-project", "url": "https://gitea.com/api/v1/repos/khainguyen1203/test-project", "link": "", "ssh_url": "git@gitea.com:khainguyen1203/test-project.git", "clone_url": "https://gitea.com/khainguyen1203/test-project.git", "original_url": "", "website": "", "stars_count": 0, "forks_count": 0, "watchers_count": 1, "open_issues_count": 0, "open_pr_counter": 0, "release_counter": 0, "default_branch": "main", "archived": false, "created_at": "2024-04-07T05:44:33Z", "updated_at": "2024-04-11T08:57:54Z", "archived_at": "1970-01-01T00:00:00Z", "permissions": {"admin": true, "push": true, "pull": true}, "has_issues": true, "internal_tracker": {"enable_time_tracker": true, "allow_only_contributors_to_track_time": true, "enable_issue_dependencies": true}, "has_wiki": true, "has_pull_requests": true, "has_projects": true, "projects_mode": "all", "has_releases": true, "has_packages": false, "has_actions": true, "ignore_whitespace_conflicts": false, "allow_merge_commits": true, "allow_rebase": true, "allow_rebase_explicit": true, "allow_squash_merge": true, "allow_fast_forward_only_merge": true, "allow_rebase_update": true, "default_delete_branch_after_merge": false, "default_merge_style": "merge", "default_allow_maintainer_edit": false, "avatar_url": "", "internal": false, "mirror_interval": "", "object_format_name": "", "mirror_updated": "0001-01-01T00:00:00Z", "repo_transfer": null}, "sender": {"id": 64084, "login": "khainguyen1203", "login_name": "", "full_name": "", "email": "khainguyen1203@noreply.gitea.com", "avatar_url": "https://seccdn.libravatar.org/avatar/a1bb6f5f3edfd0de113839ae0032b0db?d=identicon", "language": "", "is_admin": false, "last_login": "0001-01-01T00:00:00Z", "created": "2024-04-07T05:43:53Z", "restricted": false, "active": false, "prohibit_login": false, "location": "", "website": "", "description": "", "visibility": "public", "followers_count": 0, "following_count": 0, "starred_repos_count": 0, "username": "khainguyen1203"}, "commit_id": "", "review": null}', 'attributes': {'ApproximateReceiveCount': '1', 'AWSTraceHeader': 'Root=1-6617a627-659727c059be66ec741b6222;Parent=4ca3b49506eb9730;Sampled=0;Lineage=99f2516a:0', 'SentTimestamp': '1712825895525', 'SenderId': 'AROAUOGH35OSD7BV7TIOI:git-inbound-processor', 'ApproximateFirstReceiveTimestamp': '1712825895526'}, 'messageAttributes': {}, 'md5OfBody': '3764a3216fc99f1f9a300c247b3a53bf', 'eventSource': 'aws:sqs', 'eventSourceARN': 'arn:aws:sqs:ap-southeast-1:637423480371:test-project-inbound-queue', 'awsRegion': 'ap-southeast-1'}